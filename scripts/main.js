//create 2 sections
for (i=0;i<2;i++){
    let section=document.createElement('div');
    section.className="section section"+i+"";
    document.getElementById("sContainer").appendChild(section);
    // document.body.style.background="lightblue";
}
//add shirt div to section 2
let shirtContainer=document.createElement('div');
shirtContainer.className="shirtContainer";
document.querySelector(".section1").appendChild(shirtContainer);

//selection buttons
let buttonList=[['python.gif','I love python'],['chok.gif','Chok emoji'],['kenzieStrong.gif','#KenzieStrong'],['semicolon.gif','Hide and seek champion since 1958']];

let shirt=document.createElement('img');
shirt.className="shirt";
shirt.src="./assets/imgs/shirt.gif";
document.querySelector(".shirtContainer").appendChild(shirt);

//add design div to section 2
let design=document.createElement('img');
design.className="design";
design.src="./assets/imgs/"+buttonList[0][0];
document.querySelector(".shirtContainer").appendChild(design);

function showDesign(val){
    console.log(val);
    document.querySelector(".design").src="./assets/imgs/"+val+"";
}

for (i=0;i<buttonList.length;i++){
    let button=document.createElement('button');
    button.className="button";
    button.id="button"+i+"";
    button.innerHTML=buttonList[i][1];
    let b=buttonList[i][0];
    button.onclick=function showDesign1(i){
        showDesign(b);
    };
    document.querySelector(".section0").appendChild(button);
}

const ButtonHover = {
    button: document.querySelector('.button'),
    elWidth: 0,
    elHeight: 0,
    cursorX: 0,
    cursorY: 0,
    elCenterX: 0,
    elCenterY: 0,

    init() {
        this.elWidth = this.button.offsetWidth;
        this.elHeight = this.button.offsetHeight;
        this.button.addEventListener('mousemove', e => this.animate(e));
    },

    animate(e) {
        let cord = e.target.getBoundingClientRect();
        this.cursorX = e.x;
        this.cursorY = e.y;
        this.elCenterX = cord.left + (cord.width / 2);
        this.elCenterY = cord.top + (cord.height / 2);
        let y = this.elCenterY - this.cursorY;
        let x = this.elCenterX - this.cursorX;
        
        let theta = Math.atan2(y,x);
        let angle = theta * 180 / Math.PI - 90;
        if (angle < 0) {
        angle = angle + 360;
        }
        
        this.button.style.transform = 'translateX(' + (-x * 0.05) + 'px) rotateX(' + (-y * 0.1) + 'deg) rotateY(' + (x * 0.1) + 'deg)';
        this.button.style.boxShadow = x * 0.2 +"px "+ y * 0.3 +"px 28px rgba(0,0,0,0.25)";
        
    }
};

ButtonHover.init();  